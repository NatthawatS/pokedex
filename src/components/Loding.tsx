import React from "react";
const PikachuLoding: React.FC = () => {
  return (
    <img
      src="/image/gif/loading.gif"
      alt="this slowpoke moves"
      width="120px"
      height="120px"
      style={{
        borderRadius: 60,
        justifySelf: "center",
        margin: 16,
      }}
    />
  );
};
export default PikachuLoding;
