import styled from "@emotion/styled";
import React from "react";
import { breakpoint } from "../styles/breakpoint";
import Progressbar from "./Progressbar";

interface StatProps {
  name: string;
  count: number;
  total: number;
}
const Container = styled.div`
  display: grid;
  grid-template-columns: 1fr auto;
  @media ${breakpoint.desktop} {
    grid-gap: 1rem;
    grid-template-columns: 1fr auto 3fr;
    align-items: center;
  }
`;

const Text = styled.p`
  text-align: start;
  color: papayawhip;
  font-size: 13px;
  font-weight: 600;
`;

const Stat: React.FC<StatProps> = ({ name, count, total }) => {
  return (
    <Container>
      <Text>{name.toLocaleUpperCase()}</Text>
      <Text>{count}</Text>
      <Progressbar count={count} total={total}></Progressbar>
    </Container>
  );
};

export default Stat;
