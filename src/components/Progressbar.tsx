import React from "react";
import styled from "@emotion/styled";

interface ProgressbarProps {
  count?: number;
  total?: number;
}

const Progressbar: React.FC<ProgressbarProps> = ({
  count,
  total,
}): JSX.Element => {
  const usedAmount = count || 0;
  const limitAmount = total || 0;

  const getProgressBarWidthPercent = (): string => {
    let percent = 0;
    if (usedAmount > 0) {
      percent = (usedAmount / limitAmount) * 100;
    }
    return `${percent}%`;
  };

  const Container = styled.div`
    position: relative;
    height: 10px;
    border-radius: 20px;
    margin: 16px 0 8px 0;
    background: rgba(255, 255, 255, 0.12);
  `;

  interface ProgressPercentProp {
    widthPercent: string;
  }

  const ProgressPercent = styled.div<ProgressPercentProp>`
    position: absolute;
    height: 10px;
    left: 0px;
    background: #21e6c1;
    border-radius: 20px;
    width: ${(props) => (props.widthPercent ? props.widthPercent : "0%")};
  `;

  return (
    <Container>
      <ProgressPercent widthPercent={getProgressBarWidthPercent()} />
    </Container>
  );
};

export default Progressbar;
