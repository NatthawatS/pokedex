import styled from "@emotion/styled";

const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-self: center;
  justify-content: center;
`;

const DataText = styled.div`
  text-align: center;
  color: papayawhip;
  font-size: 16px;
  font-weight: 800;
`;
const DataNotFound: React.FC = () => {
  return (
    <Container>
      <img
        src="/image/gif/pikachu-cry.gif"
        alt="this slowpoke moves"
        width="120px"
        height="120px"
        style={{
          borderRadius: 20,
          margin: 16,
        }}
      />
      <DataText>Data not found.</DataText>
    </Container>
  );
};

export default DataNotFound;
