import styled from "@emotion/styled";
import React from "react";
import {
  AnimateSharedLayout,
  motion,
  useTransform,
  useViewportScroll,
} from "framer-motion";

const Paddler = styled.div`
  height: 6rem;
`;

const Wrapper = styled(motion.header)`
  position: fixed;
  z-index: 200;
  top: 0;
  width: 100vw;
  height: 6rem;
  padding: 0 4px;
  align-items: center;
  justify-content: center;
  display: flex;
  box-shadow: 0px 8px 16px #21e6c1;
  transition: box-shadow 250ms;
  transition-delay: 250ms;
  will-change: background-color;
  transition: background-color 300ms, box-shadow 300ms;
  background-color: #ffffff;
`;

const Header: React.FC = () => {
  const { scrollY } = useViewportScroll();
  const YRangeTrigger = [0, 64];

  const boxShadow = useTransform(scrollY, YRangeTrigger, [
    "0px 8px 16px rgba(112, 144, 176, 0)",
    "0px 8px 16px rgba(112, 144, 176, 0.16)",
  ]);

  return (
    <AnimateSharedLayout>
      <Paddler>
        <Wrapper
          style={{
            boxShadow: boxShadow,
          }}
        >
          <img width={200} src="/image/logo/pokemon_logo.png" alt="logo" />
        </Wrapper>
      </Paddler>
    </AnimateSharedLayout>
  );
};

export default Header;
