import styled from "@emotion/styled";
import React from "react";
import { breakpoint, size } from "../../styles/breakpoint";
import Header from "./components/Header";

export const Desktop = styled.div`
  display: none;
  @media ${breakpoint.desktop} {
    display: block;
  }
`;

export const Mobile = styled.div`
  display: block;
  @media ${breakpoint.desktop} {
    display: none;
  }
`;

export const Container = styled.div`
  display: grid;
  grid-auto-rows: max-content;
  max-width: ${size.desktop}px;
  /* padding: 0 1.25rem 0; */
  padding: 1.25rem;
  margin: 0 auto;
  @media ${breakpoint.desktop} {
    min-height: 70vh;
  }
`;

const Background = styled.div`
  background: linear-gradient(-45deg, #ee7752, #e73c7e, #23a6d5, #23d5ab);
  background-size: 400% 400%;
  animation: gradient 10s ease infinite;
  min-height: 90vh;
  /* height: 320px; */
  @keyframes gradient {
    0% {
      background-position: 0% 50%;
    }
    50% {
      background-position: 100% 50%;
    }
    100% {
      background-position: 0% 50%;
    }
  }
`;

export const Layout: React.FC = ({ children }) => {
  return (
    <>
      <Header />
      <Background>{children}</Background>
    </>
  );
};

export default Layout;
