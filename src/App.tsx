import { FC } from "react";
import Routes from "./config/routes";
import { ApolloProvider } from "@apollo/client";
import client from "./common/apollo-client";
import Providers from "./components/Providers";
import { PokedexProvider } from "./providers/PokedexContext";
import Layout from "./components/Layout";

const App: FC = () => (
  <ApolloProvider client={client}>
    <Providers providers={[PokedexProvider]}>
      <Layout>
        <Routes />
      </Layout>
    </Providers>
  </ApolloProvider>
);

export default App;
