export const size = {
  tablet: 600,
  desktop: 1200
}

export const breakpoint = {
  tablet: `only screen and (min-width: ${size.tablet}px)`,
  desktop: `only screen and (min-width: ${size.desktop}px)`
}
