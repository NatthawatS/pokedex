import styled from "@emotion/styled";
import { motion } from "framer-motion";
import funcTypes from "../../common/funcTypes";
import { breakpoint } from "../../styles/breakpoint";

export const LayoutCard = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-gap: 1rem;
  @media ${breakpoint.desktop} {
    grid-template-columns: 1fr 1fr 1fr 1fr;
  }
`;

export const Pokemons = styled(motion.div)`
  display: grid;
`;

export const NameText = styled.p`
  font-weight: bold;
  font-size: 16px;
  text-align: center;
  color: rgba(255, 255, 255, 0.7);
  @media ${breakpoint.desktop} {
    font-size: 22px;
  }
`;

export const PokemonsBox = styled.div`
  background-color: rgba(255, 255, 255, 0.7);
  border: 1px solid rgba(255, 255, 255, 0.7);
  border-radius: 100px;
  justify-self: center;
  padding: 0.5rem;
  cursor: pointer;
`;

export const PokemonImg = styled.img`
  z-index: 99;
  position: relative;
  width: 100px;
  height: 100px;
`;

export const SearchPokemon = styled.input`
  background: rgba(255, 255, 255, 0.2);
  border: none;
  outline: none;
  padding: 10px 20px;
  border-radius: 35px;
  border: 1px solid rgba(255, 255, 255, 0.5);
  border-right: 1px solid rgba(255, 255, 255, 0.2);
  border-bottom: 1px solid rgba(255, 255, 255, 0.2);
  font-size: 16px;
  letter-spacing: 1px;
  box-shadow: 0 5px 15px rgba(0, 0, 0, 0.05);
  margin-bottom: 16px;
  color: papayawhip;
  ::placeholder {
    color: papayawhip;
  }
`;

export const PokemonWrapper = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  grid-gap: 1rem;
  margin-bottom: 16px;
  @media ${breakpoint.desktop} {
    grid-template-columns: 1fr 1fr;
  }
`;

export const PokemonAvatar = styled.div`
  display: grid;
`;

export const StatisticsWrapper = styled.div`
  display: grid;
`;

export const Title = styled(NameText)`
  text-align: start;
  align-self: flex-end;
`;

export const Divider = styled.hr`
  background-color: rgba(255, 255, 255, 0.9);
  margin: 0;
`;
export const TitleBox = styled.div`
  display: grid;
  grid-template-columns: auto 1fr;
  justify-self: center;
  align-content: center;
`;

export const DreamworldWrapper = styled.div`
  display: grid;
  padding: 16px;
  justify-self: center;
  align-content: center;
`;

export const PokemonDreamworld = styled.img`
  width: 300px;
  height: 300px;
`;

export const TypeWrapper = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr 1fr 1fr;
  grid-gap: 1rem;
`;

interface TypeBoxCheckProp {
  checkType: string;
}

export const TypeBox = styled.div<TypeBoxCheckProp>`
  background: ${(props) => funcTypes(props.checkType).backgroundColors};
  box-shadow: ${(props) => funcTypes(props.checkType).boxShadowColors};
  backdrop-filter: blur(4px);
  -webkit-backdrop-filter: blur(4px);
  border-radius: 10px;
  border: 1px solid rgba(255, 255, 255, 0.18);
  padding: 8px;
  text-align: center;
  color: papayawhip;
  font-size: 16px;
  font-weight: 800;
`;
