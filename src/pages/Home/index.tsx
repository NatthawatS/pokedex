import React, { useState, useEffect, useCallback, useMemo } from "react";
import { usePokedexContext } from "../../providers/PokedexContext";
import { PokeList } from "../../interfaces/pokedex";
import { useGetPokemonDetail } from "../../hook/useGetPokemonDetail";
import { Container } from "../../components/Layout";
import { SearchPokemon } from "./home.styled";
import PikachuLoding from "../../components/Loding";
import ProfilePokemon from "./components/ProfilePokemon";
import PokemonList from "./components/PokemonList";
import DataNotFound from "../../components/DataNotFound";

const Home: React.FC = () => {
  const { pokeList, setVariable, variable, loading } = usePokedexContext();
  const [isFetching, setIsFetching] = useState(false);
  const [select, setSelect] = useState<{
    name: string;
    dreamworld: string;
  }>({
    name: "",
    dreamworld: "",
  });
  const [search, setSearch] = useState<string>("");
  const useGetPokemon = useGetPokemonDetail(select);

  const handleScroll = () => {
    if (
      Math.ceil(window.innerHeight + document.documentElement.scrollTop) !==
        document.documentElement.offsetHeight ||
      isFetching
    )
      return;
    setIsFetching(true);
  };

  const fetchMoreListItems = useCallback(() => {
    setVariable({
      limit: variable.limit,
      offset: variable.offset + 12,
    });
    setIsFetching(false);
  }, [setVariable, variable.limit, variable.offset]);

  useEffect(() => {
    window.addEventListener("scroll", handleScroll);
  }, []);

  useEffect(() => {
    if (!isFetching) return;
    fetchMoreListItems();
  }, [isFetching]);

  const filteredPokemon = useMemo((): PokeList[] => {
    return pokeList.filter(
      (pokemon) =>
        pokemon.name.toLowerCase().indexOf(search.toLowerCase()) !== -1
    );
  }, [pokeList, search]);

  return (
    <Container>
      {useGetPokemon && (
        <ProfilePokemon dataPokemon={useGetPokemon}></ProfilePokemon>
      )}

      <SearchPokemon
        value={search}
        type="text"
        placeholder="Search..."
        onChange={(e: React.ChangeEvent<HTMLInputElement>): void => {
          setSearch(e.target.value);
        }}
      />
      {filteredPokemon && filteredPokemon.length > 0 ? (
        <PokemonList
          pokemonList={filteredPokemon}
          onClicks={setSelect}
        ></PokemonList>
      ) : (
        <DataNotFound></DataNotFound>
      )}
      {loading && <PikachuLoding></PikachuLoding>}
    </Container>
  );
};

export default Home;
