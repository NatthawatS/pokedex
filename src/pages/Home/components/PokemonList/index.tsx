import React from "react";
import GlassmorphismCard from "../../../../components/GlassmorphismCard";
import { PokeList } from "../../../../interfaces/pokedex";
import {
  LayoutCard,
  NameText,
  PokemonImg,
  Pokemons,
  PokemonsBox,
} from "../../home.styled";

interface PokemonListProps {
  pokemonList: PokeList[];
  onClicks: React.Dispatch<
    React.SetStateAction<{
      name: string;
      dreamworld: string;
    }>
  >;
}

const PokemonList: React.FC<PokemonListProps> = ({ pokemonList, onClicks }) => {
  return (
    <GlassmorphismCard>
      <LayoutCard>
        {pokemonList &&
          pokemonList.length > 0 &&
          pokemonList.map((data) => (
            <Pokemons
              key={data.id}
              onClick={() => {
                onClicks({ name: data.name, dreamworld: data.dreamworld });
                document.body.scrollTop = 0;
                document.documentElement.scrollTop = 0;
              }}
              whileHover={{ scale: 1.1 }}
              whileTap={{ scale: 0.9 }}
            >
              <PokemonsBox>
                <PokemonImg src={data.image}></PokemonImg>
              </PokemonsBox>
              <NameText>{data.name.toUpperCase()}</NameText>
            </Pokemons>
          ))}
      </LayoutCard>
    </GlassmorphismCard>
  );
};

export default PokemonList;
