import GlassmorphismCard from "../../../../components/GlassmorphismCard";
import Stat from "../../../../components/Stat";
import { PokemonDetail } from "../../../../interfaces/pokedex";
import {
  Divider,
  DreamworldWrapper,
  PokemonAvatar,
  PokemonDreamworld,
  PokemonWrapper,
  StatisticsWrapper,
  Title,
  TitleBox,
  TypeBox,
  TypeWrapper,
} from "../../home.styled";

interface ProfilePokemonProps {
  dataPokemon: PokemonDetail;
}
const ProfilePokemon: React.FC<ProfilePokemonProps> = ({ dataPokemon }) => {
  return (
    <PokemonWrapper>
      <PokemonAvatar>
        {dataPokemon?.dreamworld && (
          <>
            <DreamworldWrapper>
              <PokemonDreamworld
                src={dataPokemon.dreamworld}
              ></PokemonDreamworld>
            </DreamworldWrapper>
            <TitleBox>
              <Title>
                #{dataPokemon.id}: {dataPokemon.name.toUpperCase()}
              </Title>
            </TitleBox>
          </>
        )}
      </PokemonAvatar>
      <GlassmorphismCard>
        <StatisticsWrapper>
          <Title>Base Stats</Title>
          {dataPokemon.stats.map((data) => {
            return (
              <Stat
                key={data.stat.name}
                name={data.stat.name}
                count={+data.base_stat}
                total={200}
              />
            );
          })}
          <Divider />
          <Title>Type</Title>
          <TypeWrapper>
            {dataPokemon.types.map((data, index) => (
              <TypeBox checkType={data.type.name} key={index}>
                {data.type.name}
              </TypeBox>
            ))}
          </TypeWrapper>
        </StatisticsWrapper>
      </GlassmorphismCard>
    </PokemonWrapper>
  );
};
export default ProfilePokemon;
