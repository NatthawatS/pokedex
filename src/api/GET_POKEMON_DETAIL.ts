import { gql } from "@apollo/client";

export const GET_POKEMON_DETAIL = gql`
  query pokemon($name: String!) {
    pokemon(name: $name) {
      abilities {
        ability {
          name
        }
        is_hidden
        slot
      }
      base_experience
      height
      id
      is_default
      name
      order
      species {
        id
        url
        name
      }
      sprites {
        front_default
      }
      stats {
        base_stat
        effort
        stat {
          name
        }
      }
      types {
        type {
          name
        }
      }
      weight
      status
      message
    }
  }
`;
