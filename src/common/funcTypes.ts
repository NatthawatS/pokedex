import { TypesPokemon } from "../interfaces/pokedex";

const useFuncTypes = (valueType: string) => {
  switch (valueType) {
    case TypesPokemon.normal:
      return {
        backgroundColors: "rgba(169,170,153, 0.25)",
        boxShadowColors: "0 8px 32px 0 rgba(169,170,153, 0.37)",
      };
    case TypesPokemon.fighting:
      return {
        backgroundColors: "rgba(187,85,68, 0.25)",
        boxShadowColors: "0 8px 32px 0 rgba(187,85,68, 0.37)",
      };
    case TypesPokemon.flying:
      return {
        backgroundColors: "rgba(136,153,255, 0.25)",
        boxShadowColors: "0 8px 32px 0 rgba(136,153,255, 0.37)",
      };
    case TypesPokemon.poison:
      return {
        backgroundColors: "rgba(170,85,153, 0.25)",
        boxShadowColors: "0 8px 32px 0 rgba(170,85,153, 0.37)",
      };
    case TypesPokemon.ground:
      return {
        backgroundColors: "rgba(220,187,84, 0.25)",
        boxShadowColors: "0 8px 32px 0 rgba(220,187,84, 0.37)",
      };
    case TypesPokemon.rock:
      return {
        backgroundColors: "rgba(187,170,102, 0.25)",
        boxShadowColors: "0 8px 32px 0 rgba(187,170,102, 0.37)",
      };
    case TypesPokemon.bug:
      return {
        backgroundColors: "rgba(170,186,34, 0.25)",
        boxShadowColors: "0 8px 32px 0 rgba(170,186,34, 0.37)",
      };
    case TypesPokemon.ghost:
      return {
        backgroundColors: "rgba(102,102,186, 0.25)",
        boxShadowColors: "0 8px 32px 0 rgba(102,102,186, 0.37)",
      };
    case TypesPokemon.steel:
      return {
        backgroundColors: "rgba(170,170,187, 0.25)",
        boxShadowColors: "0 8px 32px 0 rgba(170,170,187, 0.37)",
      };
    case TypesPokemon.fire:
      return {
        backgroundColors: "rgba(255,67,33, 0.25)",
        boxShadowColors: "0 8px 32px 0 rgba(255,67,33, 0.37)",
      };
    case TypesPokemon.water:
      return {
        backgroundColors: "rgba(52,153,254, 0.25)",
        boxShadowColors: "0 8px 32px 0 rgba(52,153,254, 0.37)",
      };
    case TypesPokemon.grass:
      return {
        backgroundColors: "rgba(119,204,85, 0.25)",
        boxShadowColors: "0 8px 32px 0 rgba(119,204,85, 0.37)",
      };
    case TypesPokemon.electric:
      return {
        backgroundColors: "rgba(255,204,50, 0.25)",
        boxShadowColors: "0 8px 32px 0 rgba(255,204,50, 0.37)",
      };
    case TypesPokemon.psychic:
      return {
        backgroundColors: "rgba(255,86,153, 0.25)",
        boxShadowColors: "0 8px 32px 0 rgba(255,86,153, 0.37)",
      };
    case TypesPokemon.ice:
      return {
        backgroundColors: "rgba(102,205,255, 0.25)",
        boxShadowColors: "0 8px 32px 0 rgba(102,205,255, 0.37)",
      };
    case TypesPokemon.dragon:
      return {
        backgroundColors: "rgba(119,102,238, 0.25)",
        boxShadowColors: "0 8px 32px 0 rgba(119,102,238, 0.37)",
      };
    case TypesPokemon.dark:
      return {
        backgroundColors: "rgba(120,85,68, 0.25)",
        boxShadowColors: "0 8px 32px 0 rgba(120,85,68, 0.37)",
      };
    case TypesPokemon.fairy:
      return {
        backgroundColors: "rgba(238,153,238, 0.25)",
        boxShadowColors: "0 8px 32px 0 rgba(238,153,238, 0.37)",
      };
    case TypesPokemon.unknown:
      return {
        backgroundColors: "rgba(255, 255, 255, 0.25)",
        boxShadowColors: "0 8px 32px 0 rgba(31, 38, 135, 0.37)",
      };
    case TypesPokemon.shadow:
      return {
        backgroundColors: "rgba(255, 255, 255, 0.25)",
        boxShadowColors: "0 8px 32px 0 rgba(31, 38, 135, 0.37)",
      };

    default:
      return {
        backgroundColors: "rgba(255, 255, 255, 0.25)",
        boxShadowColors: "0 8px 32px 0 rgba(31, 38, 135, 0.37)",
      };
  }
};

export default useFuncTypes;
