import { useQuery } from "@apollo/client";
import { GET_POKEMON_DETAIL } from "../api/GET_POKEMON_DETAIL";
import { PokemonDetail } from "../interfaces/pokedex";

export const useGetPokemonDetail = (variable: {
  name: string;
  dreamworld: string;
}): PokemonDetail | undefined => {
  const { data, loading } = useQuery(GET_POKEMON_DETAIL, {
    variables: { name: variable.name },
  });
  if (loading) {
    return undefined;
  }
  if (data.pokemon.id === null) return undefined;
  const dreamworld = variable.dreamworld;
  return { ...data?.pokemon, dreamworld: dreamworld };
};
