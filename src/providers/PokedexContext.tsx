import {
  createContext,
  useContext,
  useEffect,
  useState,
  useCallback,
  SetStateAction,
  Dispatch,
} from "react";
import { useApolloClient } from "@apollo/client";
import { GET_POKEMONS } from "../api/GET_POKEMONS";
import { PokeList, Variables, EnumVariables } from "../interfaces/pokedex";

interface Props {
  pokeList: PokeList[];
  loading: boolean;
  refresh: () => void;
  variable: Variables;
  setVariable: Dispatch<SetStateAction<Variables>>;
}

const PokedexContext = createContext<Props>({
  pokeList: [],
  loading: false,
  refresh: () => null,
  variable: {
    limit: EnumVariables.Limit,
    offset: EnumVariables.Offset,
  },
  setVariable: () => {},
});

export const usePokedexContext = (): Props => {
  const { pokeList, loading, refresh, variable, setVariable } =
    useContext<Props>(PokedexContext);
  return {
    pokeList,
    loading,
    refresh,
    variable,
    setVariable,
  };
};

export const PokedexProvider: React.FC = ({ children }) => {
  const [loading, setLoading] = useState<boolean>(false);
  const [pokeList, setPokeList] = useState<PokeList[]>([]);
  const [variable, setVariable] = useState<Variables>({
    limit: EnumVariables.Limit,
    offset: EnumVariables.Offset,
  });

  const client = useApolloClient();
  const refresh = useCallback(() => {
    const getPokeApi = async () => {
      setLoading(true);
      const queryPokemons = await client.query({
        query: GET_POKEMONS,
        variables: variable,
      });

      const quertPokemon = queryPokemons?.data?.pokemons?.results || [];
      setPokeList([...pokeList, ...quertPokemon]);
      setLoading(false);
    };
    getPokeApi();
  }, [client, variable]);

  useEffect(() => refresh(), [refresh]);

  const contextValue = {
    pokeList,
    loading,
    refresh,
    variable,
    setVariable,
  };

  return (
    <PokedexContext.Provider value={contextValue}>
      {children}
    </PokedexContext.Provider>
  );
};
