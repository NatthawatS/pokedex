export interface PokeList {
  id: string;
  url: string;
  name: string;
  image: string;
  artwork: string;
  dreamworld: string;
}

interface Ability {
  name: string;
}
interface Abilities {
  ability: Ability;
  is_hidden: string;
  slot: string;
}

interface Species {
  id: string;
  url: string;
  name: string;
}

interface Sprites {
  front_default: string;
}

interface Stat {
  name: string;
}
interface Stats {
  base_stat: string;
  effort: string;
  stat: Stat;
}

interface Types {
  type: {
    name: string;
  };
}
export interface PokemonDetail {
  id: string;
  abilities: Abilities[];
  base_experience: string;
  height: string;
  is_default: string;
  name: string;
  order: string;
  species: Species;
  sprites: Sprites;
  stats: Stats[];
  types: Types[];
  weight: string;
  status: string;
  message: string;
  dreamworld?: string;
}

export interface Variables {
  limit: number;
  offset: number;
}

export enum EnumVariables {
  Limit = 12,
  Offset = 0,
}

export enum TypesPokemon {
  normal = "normal",
  fighting = "fighting",
  flying = "flying",
  poison = "poison",
  ground = "ground",
  rock = "rock",
  bug = "bug",
  ghost = "ghost",
  steel = "steel",
  fire = "fire",
  water = "water",
  grass = "grass",
  electric = "electric",
  psychic = "psychic",
  ice = "ice",
  dragon = "dragon",
  dark = "dark",
  fairy = "fairy",
  unknown = "unknown",
  shadow = "shadow",
}
