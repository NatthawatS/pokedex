
# Project Pokedex 

- gitlab repo : https://gitlab.com/NatthawatS/pokedex
- website : https://pokedex-2ns.web.app/
## GraphqlApi
- api : https://graphql-pokeapi.vercel.app/
- git repo api : https://github.com/mazipan/graphql-pokeapi

## Tools

### React+Typescript 
- React hook คือ function ที่ให้เราสามารถใช้ react features ได้ เช่น ให้เราสามารถเรียกใช้ state ได้ โดยที่ไม่ต้องใช้การ implement Class 
  
### apollo
- Apollo Client Apollo Client เป็นไลบรารีไคลเอ็นต์ GraphQL ที่พร้อมใช้งานสำหรับเฟรมเวิร์ก JavaScript
### emotion
- Emotion is a library designed สำหรับจัดการเขียน css styles  
### framer-motion
- Animation css สำหรับสร้าง Animation css ได้ง่ายขึ้น

### firebase 
- Firebase Hosting เป็นบริการ Web Hosting 


## Run Project

- step 1 git clone repo

```
 git clone https://gitlab.com/NatthawatS/pokedex.git
```
- step 2 yarn

```
 yarn
```
- step 3 start project

```
 yarn start

```

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**
